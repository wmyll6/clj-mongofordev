(ns m101.views.welcome
  (:require [m101.views.common :as common]
            [noir.content.getting-started]
            [noir.session :as session])
  (:use [noir.core :only [defpage render]]
        hiccup.core hiccup.form))

(defpage "/" {:keys [handle error]} 
  (common/layout
   [:p "Welcome to 'MongoDB For Developers'"]
   [:ul
    (for [thing ["apple" "oragne" "peach"]]
      [:li thing])]
   ; favorite fruit form
   (form-to [:post "/favorite_fruit"]
            (label "fruit" "Your favorite fruit is: ") [:br]
            (text-field "fruit" handle) [:br]
            (submit-button "submit"))))

(defpage [:post "/favorite_fruit"] {:keys [fruit]}
  (session/put! :fruit fruit)
  (render "/show_fruit"))

(defpage "/show_fruit" []
  (common/layout
   (let [fruit (session/get :fruit)]
     [:p (str "Your favorite fruit is: " fruit)])))
