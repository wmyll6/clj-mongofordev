# Ported to Clojure exercises from 10gen MongoDB for Developers [free course][c]

    Clojure ecosystem used instead of python. Authors of the course suggest such 
[bottle][b] framework for web development and *pymongo* as a Mong Driver* as a
Mong Driver. This port uses corresponding alternatives

* [Bottle][b] -> [Noir][n]
* [PyMongo][m] -> [clj-mongodb][1] (is a wrapper around the Java MongoDB Driver)

## Noir
Noir is a micro-framework that allows you to rapidly develop websites in Clojure.

[Information][2] how to install/configure Noir
[Enlive for Noir Templates](http://paulosuzart.github.com/blog/2012/03/25/web-noir-plus-enlive-template/)

## Mongo 
Found two clojure drivers for Mongo

* clj-mongo
* monger
** [Article](http://clojuremongodb.info/articles/updating.html)
** [Basic Noir Integration Example](http://clojuremongodb.info/articles/integration.html)


[Example][4] of application where Mongo DB used
[4]: https://github.com/xavi/noir-auth-app

## Create Project

```
lein noir new clj-mongodbfordev

```
 
[1]: https://github.com/jeffh/clj-mongodb
[2]: http://yogthos.net/blog/22
[c]: https://education.10gen.com/courses/10gen/M101/2012_Fall/about
[b]: http://bottlepy.org/docs/dev/
[n]: http://webnoir.org/
[m]: http://api.mongodb.org/python/current/
